############################## Code: /opt/pytorch ##############################
FROM bitnami/git as code

ARG TORCH_TAG="v1.10.2"

RUN git clone https://github.com/pytorch/pytorch /opt/pytorch && \
    cd /opt/pytorch && \
    git checkout "tags/${TORCH_TAG}" -b "local" && \
    git submodule sync && \
    git submodule update --init --recursive --jobs 0 && \
    rm -fr .git

############################## Conda: /opt/conda ###############################
FROM ubuntu:20.04 as conda

ARG INSTALLER="Miniconda3-py38_4.10.3-Linux-x86_64.sh"
ARG CONDA_PREFIX="/opt/conda"

RUN apt update -y && apt install -y wget
RUN cd /tmp && \
    wget "https://repo.anaconda.com/miniconda/${INSTALLER}" && \
    bash "${INSTALLER}" -b -p "${CONDA_PREFIX}" && \
    /opt/conda/bin/conda install \
        astunparse numpy ninja pyyaml mkl mkl-include setuptools cffi \
        typing_extensions future six requests dataclasses

#################################### Builder ###################################
FROM ubuntu:20.04
ENV DEBIAN_FRONTEND=noninteractive

COPY --from=code /opt/pytorch /opt/pytorch
COPY --from=conda /opt/conda /opt/conda

# Build pytorch from source: https://github.com/pytorch/pytorch#from-source
RUN apt update -y && apt install -y cmake g++ time && apt clean -y
ENV CMAKE_PREFIX_PATH=/opt/conda
WORKDIR /opt/pytorch
ENTRYPOINT \
    BUILD_TEST=0 \
    USE_CUDA=OFF \
    USE_CUDNN=OFF \
    USE_DISTRIBUTED=0 \
    USE_FAKELOWP=0 \
    USE_FBGEMM=0 \
    USE_MKLDNN=0 \
    USE_NNPACK=0 \
    USE_PYTORCH_QNNPACK=0 \
    USE_QNNPACK=0 \
    USE_XNNPACK=0 \
    time /opt/conda/bin/python setup.py develop
