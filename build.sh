#!/usr/bin/env bash

TORCH_TAG="v1.10.2"
IMAGE="xiangquan/torchmark:${TORCH_TAG}"
DOCKER_BUILDKIT=1 docker build --network="host" --build-arg "TORCH_TAG=${TORCH_TAG}" -t ${IMAGE} .
docker push ${IMAGE}

LATEST="xiangquan/torchmark:latest"
docker tag ${IMAGE} ${LATEST}
docker push ${LATEST}
