# TorchMark: Benchmark your PC by building Pytorch

Many benchmarks are for gamers, designers, etc, while TorchMark is a more meaningful result for
developers who generally work with make, CMake or Bazel to build software.

## Usage

```bash
docker run xiangquan/torchmark
```

## Results

Compared with [PassMark](https://www.cpubenchmark.net/cpu_list.php) (Higher is better).

|Product|OS     |CPU                |PassMark|Memory       |TorchMark|
|-------|-------|-------------------|--------|-------------|---------|
|Desktop|Ubuntu |i9-13900, 24C/32T  |47977   |64G, 2x 5200M|470      |
|Desktop|Ubuntu |r7-5700G, 8C/16T   |24591   |16G, 2x 3200M|1096     |
|Desktop|Ubuntu |i7-8700, 6C/12T    |13048   |64G, 2x 3200M|1346     |
|Desktop|Ubuntu |i7-8700, 6C/12T    |13048   |32G, 2x 3200M|1351     |
|Laptop |WSL    |i5-10300H, 4C/8T   |8716    |32G, 2x 2933M|2290     |
|Laptop |Ubuntu |i7-6820HQ, 4C/8T   |6945    |32G, 2x 2133M|2545     |
|Laptop |WSL    |i5-10300H, 4C/8T   |8716    |16G, 1x 2933M|2634     |
|Laptop |Ubuntu |i5-10300H, 4C/8T   |8716    |32G, 2x 2933M|2721     |
|Laptop |Penguin|i3-1115G4, 2C/4T   |6232    |8G, 2x 3733M |5357     |
